<?xml version="1.0" encoding="UTF-8"?>
<?xml-model href="http://schemata.earlyprint.org/schemata/tei_earlyprint.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="A75759">
 <teiHeader>
  <fileDesc>
   <titleStmt>
    <title>At a meeting of the Committee of Arrears the eleventh day of December, 1648.</title>
    <author>City of London (England). Court of Common Council. Committee for Arrears.</author>
   </titleStmt>
   <editionStmt>
    <edition>This text is an enriched version of the TCP digital transcription A75759 of text R211092 in the <ref target="http://estc.bl.uk">English Short Title Catalog</ref> (Thomason 669.f.13[54]). Textual changes  and metadata enrichments aim at making the text more  computationally tractable, easier to read, and suitable for network-based collaborative curation by amateur and professional end users from many walks of life.  The text has been tokenized and linguistically annotated with <ref target="http://morphadorner.northwestern.edu/morphadorner/">MorphAdorner</ref>. The annotation includes standard spellings that support the display of a text in a standardized format that preserves archaic forms ('loveth', 'seekest'). Textual changes aim at restoring the text the author or stationer meant to publish.</edition>
    <respStmt>
     <resp>This text has not been fully proofread</resp>
     <name/>
    </respStmt>
   </editionStmt>
   <extent>Approx. 2 KB of XML-encoded text transcribed from 1 1-bit group-IV TIFF page image.</extent>
   <publicationStmt>
    <publisher>EarlyPrint Project</publisher>
    <pubPlace>Evanston,IL, Notre Dame, IN, St. Louis, MO</pubPlace>
    <date>2017</date>
    <idno type="DLPS">A75759</idno>
    <idno type="STC">Wing A4095</idno>
    <idno type="STC">Thomason 669.f.13[54]</idno>
    <idno type="STC">ESTC R211092</idno>
    <idno type="EEBO-CITATION">99869830</idno>
    <idno type="PROQUEST">99869830</idno>
    <idno type="VID">162950</idno>
    <idno type="PROQUESTGOID">2240943421</idno>
    <availability>
     <p>
            This keyboarded and encoded edition of the work described above is co-owned by the institutions providing financial support to the Early English Books Online Text Creation Partnership. This Phase I text is available for reuse, according to the terms of
            <ref target="https://creativecommons.org/publicdomain/zero/1.0/">Creative Commons 0 1.0 Universal</ref>
            . The text can be copied, modified, distributed and performed, even for commercial purposes, all without asking permission.
          </p>
    </availability>
   </publicationStmt>
   <seriesStmt>
    <title>Early English books online.</title>
   </seriesStmt>
   <notesStmt>
    <note>(EEBO-TCP ; phase 1, no. A75759)</note>
    <note>Transcribed from: (Early English Books Online ; image set 162950)</note>
    <note>Images scanned from microfilm: (Thomason Tracts ; 246:669f13[54])</note>
   </notesStmt>
   <sourceDesc>
    <biblFull>
     <titleStmt>
      <title>At a meeting of the Committee of Arrears the eleventh day of December, 1648.</title>
      <author>City of London (England). Court of Common Council. Committee for Arrears.</author>
      <author>Lathum, Tho.</author>
     </titleStmt>
     <extent>1 sheet ([1] p.)</extent>
     <publicationStmt>
      <publisher>s.n.,</publisher>
      <pubPlace>[London :</pubPlace>
      <date>1648]</date>
     </publicationStmt>
     <notesStmt>
      <note>Imprint from Wing.</note>
      <note>Signed at end: Tho. Lathum Clerk to the said Committee.</note>
      <note>Reproduction of the original in the British Library.</note>
     </notesStmt>
    </biblFull>
   </sourceDesc>
  </fileDesc>
  <profileDesc>
   <langUsage>
    <language ident="eng">eng</language>
   </langUsage>
   <textClass>
    <keywords scheme="http://authorities.loc.gov/">
     <term>Taxation -- England -- London -- Early works to 1800.</term>
     <term>Great Britain -- History -- Civil War, 1642-1649 -- Finance -- Early works to 1800.</term>
     <term>London (England) -- Politics and government -- 17th century -- Early works to 1800.</term>
    </keywords>
   </textClass>
  </profileDesc>
  <xenoData>
   <ep:epHeader xmlns:ep="http://earlyprint.org/ns/1.0">
    <ep:tcp>A75759</ep:tcp>
    <ep:estc> R211092</ep:estc>
    <ep:stc> (Thomason 669.f.13[54]). </ep:stc>
    <ep:corpus>civilwar</ep:corpus>
    <ep:workpart>no</ep:workpart>
    <ep:title>At a meeting of the Committee of Arrears the eleventh day of December, 1648.</ep:title>
    <ep:author>City of London</ep:author>
    <ep:publicationYear>1648</ep:publicationYear>
    <ep:creationYear/>
    <ep:curator>
     <ep:name/>
    </ep:curator>
    <ep:pageCount>1</ep:pageCount>
    <ep:wordCount>285</ep:wordCount>
    <ep:defectiveTokenCount>0</ep:defectiveTokenCount>
    <ep:untranscribedForeignCount>0</ep:untranscribedForeignCount>
    <ep:untranscribedMathCount>0</ep:untranscribedMathCount>
    <ep:untranscribedMusicCount>0</ep:untranscribedMusicCount>
    <ep:missingChunkCount>0</ep:missingChunkCount>
    <ep:missingPagesCount>0</ep:missingPagesCount>
    <ep:defectRate>0</ep:defectRate>
    <ep:finalGrade>A</ep:finalGrade>
    <ep:defectRangePerGrade>This text  has no known defects that were recorded as gap elements at the time of transcription. </ep:defectRangePerGrade>
   </ep:epHeader>
  </xenoData>
  <revisionDesc>
   <change>
    <date>2008-06</date>
    <label>TCP</label>
        Assigned for keying and markup
      </change>
   <change>
    <date>2008-07</date>
    <label>SPi Global</label>
        Keyed and coded from ProQuest page images
      </change>
   <change>
    <date>2008-09</date>
    <label>John Pas</label>
        Sampled and proofread
      </change>
   <change>
    <date>2008-09</date>
    <label>John Pas</label>
        Text and markup reviewed and edited
      </change>
   <change>
    <date>2009-02</date>
    <label>pfs</label>
        Batch review (QC) and XML conversion
      </change>
  </revisionDesc>
 </teiHeader>
 <text xml:id="A75759-e10">
  <body xml:id="A75759-e20">
   <pb facs="tcp:162950:1" rend="simple:additions" xml:id="A75759-001-a"/>
   <div type="text" xml:id="A75759-e30">
    <head xml:id="A75759-e40">
     <w lemma="at" pos="acp" xml:id="A75759-001-a-0010">At</w>
     <w lemma="a" pos="d" xml:id="A75759-001-a-0020">a</w>
     <w lemma="meeting" pos="n1" xml:id="A75759-001-a-0030">meeting</w>
     <w lemma="of" pos="acp" xml:id="A75759-001-a-0040">of</w>
     <w lemma="the" pos="d" xml:id="A75759-001-a-0050">the</w>
     <w lemma="committee" pos="n1" xml:id="A75759-001-a-0060">Committee</w>
     <w lemma="of" pos="acp" xml:id="A75759-001-a-0070">of</w>
     <w lemma="arrear" pos="n2" xml:id="A75759-001-a-0080">Arrears</w>
     <w lemma="the" pos="d" xml:id="A75759-001-a-0090">the</w>
     <w lemma="eleven" pos="ord" xml:id="A75759-001-a-0100">eleventh</w>
     <w lemma="day" pos="n1" xml:id="A75759-001-a-0110">day</w>
     <w lemma="of" pos="acp" xml:id="A75759-001-a-0120">of</w>
     <hi xml:id="A75759-e50">
      <w lemma="December" pos="nn1" xml:id="A75759-001-a-0130">December</w>
      <pc xml:id="A75759-001-a-0140">,</pc>
     </hi>
     <w lemma="1648." pos="crd" xml:id="A75759-001-a-0150">1648.</w>
     <pc unit="sentence" xml:id="A75759-001-a-0160"/>
    </head>
    <p xml:id="A75759-e60">
     <w lemma="it" pos="pn" rend="decorinit" xml:id="A75759-001-a-0170">IT</w>
     <w lemma="be" pos="vvz" xml:id="A75759-001-a-0180">is</w>
     <w lemma="order" pos="vvn" xml:id="A75759-001-a-0190">Ordered</w>
     <w lemma="by" pos="acp" xml:id="A75759-001-a-0200">by</w>
     <w lemma="the" pos="d" xml:id="A75759-001-a-0210">the</w>
     <w lemma="say" pos="j-vn" xml:id="A75759-001-a-0220">said</w>
     <w lemma="committee" pos="n1" xml:id="A75759-001-a-0230">Committee</w>
     <pc xml:id="A75759-001-a-0240">,</pc>
     <w lemma="that" pos="cs" xml:id="A75759-001-a-0250">that</w>
     <w lemma="the" pos="d" xml:id="A75759-001-a-0260">the</w>
     <w lemma="ward" pos="n1" xml:id="A75759-001-a-0270">Ward</w>
     <w lemma="book" pos="n2" xml:id="A75759-001-a-0280">books</w>
     <w lemma="of" pos="acp" xml:id="A75759-001-a-0290">of</w>
     <w lemma="the" pos="d" xml:id="A75759-001-a-0300">the</w>
     <w lemma="several" pos="j" reg="several" xml:id="A75759-001-a-0310">severall</w>
     <w lemma="arrear" pos="n2" xml:id="A75759-001-a-0320">Arrears</w>
     <w lemma="remain" pos="vvg" xml:id="A75759-001-a-0330">remaining</w>
     <w lemma="unpaid" pos="j" xml:id="A75759-001-a-0340">unpaid</w>
     <w lemma="upon" pos="acp" xml:id="A75759-001-a-0350">upon</w>
     <w lemma="the" pos="d" xml:id="A75759-001-a-0360">the</w>
     <w lemma="several" pos="j" reg="several" xml:id="A75759-001-a-0370">severall</w>
     <w lemma="assessment" pos="n2" reg="assessments" xml:id="A75759-001-a-0380">Assessements</w>
     <w lemma="make" pos="vvn" xml:id="A75759-001-a-0390">made</w>
     <w lemma="for" pos="acp" xml:id="A75759-001-a-0400">for</w>
     <w lemma="the" pos="d" xml:id="A75759-001-a-0410">the</w>
     <w lemma="maintenance" pos="n1" xml:id="A75759-001-a-0420">maintenance</w>
     <w lemma="of" pos="acp" xml:id="A75759-001-a-0430">of</w>
     <w lemma="the" pos="d" xml:id="A75759-001-a-0440">the</w>
     <w lemma="army" pos="n1" xml:id="A75759-001-a-0450">Army</w>
     <w lemma="under" pos="acp" xml:id="A75759-001-a-0460">under</w>
     <w lemma="the" pos="d" xml:id="A75759-001-a-0470">the</w>
     <w lemma="command" pos="n1" xml:id="A75759-001-a-0480">Command</w>
     <w lemma="of" pos="acp" xml:id="A75759-001-a-0490">of</w>
     <w lemma="his" pos="po" xml:id="A75759-001-a-0500">his</w>
     <w lemma="excellency" pos="n1" xml:id="A75759-001-a-0510">Excellency</w>
     <w lemma="the" pos="d" xml:id="A75759-001-a-0520">the</w>
     <w lemma="lord" pos="n1" xml:id="A75759-001-a-0530">Lord</w>
     <hi xml:id="A75759-e70">
      <w lemma="Fairfax" pos="nn1" xml:id="A75759-001-a-0540">Fairfax</w>
      <pc xml:id="A75759-001-a-0550">,</pc>
     </hi>
     <w lemma="now" pos="av" xml:id="A75759-001-a-0560">now</w>
     <w lemma="in" pos="acp" xml:id="A75759-001-a-0570">in</w>
     <w lemma="the" pos="d" xml:id="A75759-001-a-0580">the</w>
     <w lemma="custody" pos="n1" xml:id="A75759-001-a-0590">custody</w>
     <w lemma="of" pos="acp" xml:id="A75759-001-a-0600">of</w>
     <w lemma="this" pos="d" xml:id="A75759-001-a-0610">this</w>
     <w lemma="committee" pos="n1" xml:id="A75759-001-a-0620">Committee</w>
     <pc xml:id="A75759-001-a-0630">,</pc>
     <w lemma="be" pos="vvb" xml:id="A75759-001-a-0640">be</w>
     <w lemma="herewith" pos="av" xml:id="A75759-001-a-0650">herewith</w>
     <w lemma="return" pos="vvn" xml:id="A75759-001-a-0660">returned</w>
     <w lemma="to" pos="acp" xml:id="A75759-001-a-0670">to</w>
     <w lemma="the" pos="d" xml:id="A75759-001-a-0680">the</w>
     <w lemma="deputy" pos="n1" xml:id="A75759-001-a-0690">Deputy</w>
     <w lemma="and" pos="cc" xml:id="A75759-001-a-0700">and</w>
     <w lemma="common-council" pos="n1" reg="common-council" xml:id="A75759-001-a-0710">Common-councell</w>
     <w lemma="man" pos="n2" xml:id="A75759-001-a-0720">men</w>
     <w lemma="of" pos="acp" xml:id="A75759-001-a-0730">of</w>
     <w lemma="the" pos="d" xml:id="A75759-001-a-0740">the</w>
     <w lemma="respective" pos="j" xml:id="A75759-001-a-0750">respective</w>
     <w lemma="ward" pos="n2" xml:id="A75759-001-a-0760">Wards</w>
     <pc xml:id="A75759-001-a-0770">;</pc>
     <w lemma="with" pos="acp" xml:id="A75759-001-a-0780">with</w>
     <w lemma="direction" pos="n2" xml:id="A75759-001-a-0790">directions</w>
     <w lemma="that" pos="cs" xml:id="A75759-001-a-0800">that</w>
     <w lemma="they" pos="pns" xml:id="A75759-001-a-0810">they</w>
     <w lemma="shall" pos="vmb" xml:id="A75759-001-a-0820">shall</w>
     <w lemma="within" pos="acp" xml:id="A75759-001-a-0830">within</w>
     <w lemma="two" pos="crd" xml:id="A75759-001-a-0840">two</w>
     <w lemma="day" pos="n2" reg="days" xml:id="A75759-001-a-0850">dayes</w>
     <pc xml:id="A75759-001-a-0860">,</pc>
     <w lemma="call" pos="vvb" xml:id="A75759-001-a-0870">call</w>
     <w lemma="all" pos="d" xml:id="A75759-001-a-0880">all</w>
     <w lemma="the" pos="d" xml:id="A75759-001-a-0890">the</w>
     <w lemma="collector" pos="n2" xml:id="A75759-001-a-0900">Collectors</w>
     <w lemma="of" pos="acp" xml:id="A75759-001-a-0910">of</w>
     <w lemma="their" pos="po" xml:id="A75759-001-a-0920">their</w>
     <w lemma="ward" pos="n1" xml:id="A75759-001-a-0930">Ward</w>
     <w lemma="before" pos="acp" xml:id="A75759-001-a-0940">before</w>
     <w lemma="they" pos="pno" xml:id="A75759-001-a-0950">them</w>
     <pc xml:id="A75759-001-a-0960">,</pc>
     <w lemma="and" pos="cc" xml:id="A75759-001-a-0970">and</w>
     <w lemma="examine" pos="vvi" xml:id="A75759-001-a-0980">examine</w>
     <w lemma="their" pos="po" xml:id="A75759-001-a-0990">their</w>
     <w lemma="roll" pos="n2" xml:id="A75759-001-a-1000">rolls</w>
     <w lemma="with" pos="acp" xml:id="A75759-001-a-1010">with</w>
     <w lemma="the" pos="d" xml:id="A75759-001-a-1020">the</w>
     <w lemma="say" pos="j-vn" xml:id="A75759-001-a-1030">said</w>
     <w lemma="book" pos="n2" xml:id="A75759-001-a-1040">books</w>
     <pc xml:id="A75759-001-a-1050">,</pc>
     <w lemma="and" pos="cc" xml:id="A75759-001-a-1060">and</w>
     <w lemma="cross" pos="vvi" xml:id="A75759-001-a-1070">cross</w>
     <w lemma="the" pos="d" xml:id="A75759-001-a-1080">the</w>
     <w lemma="same" pos="d" xml:id="A75759-001-a-1090">same</w>
     <w lemma="book" pos="n2" xml:id="A75759-001-a-1100">books</w>
     <w lemma="for" pos="acp" xml:id="A75759-001-a-1110">for</w>
     <w lemma="so" pos="av" xml:id="A75759-001-a-1120">so</w>
     <w lemma="much" pos="av-d" xml:id="A75759-001-a-1130">much</w>
     <w lemma="as" pos="acp" xml:id="A75759-001-a-1140">as</w>
     <w lemma="be" pos="vvb" xml:id="A75759-001-a-1150">are</w>
     <w lemma="pay" pos="vvn" xml:id="A75759-001-a-1160">paid</w>
     <pc xml:id="A75759-001-a-1170">;</pc>
     <w lemma="and" pos="cc" xml:id="A75759-001-a-1180">and</w>
     <w lemma="return" pos="vvb" reg="return" xml:id="A75759-001-a-1190">returne</w>
     <w lemma="the" pos="d" xml:id="A75759-001-a-1200">the</w>
     <w lemma="same" pos="d" xml:id="A75759-001-a-1210">same</w>
     <w lemma="book" pos="n2" xml:id="A75759-001-a-1220">books</w>
     <w lemma="again" pos="av" xml:id="A75759-001-a-1230">again</w>
     <w lemma="within" pos="acp" xml:id="A75759-001-a-1240">within</w>
     <w lemma="two" pos="crd" xml:id="A75759-001-a-1250">two</w>
     <w lemma="day" pos="n2" reg="days" xml:id="A75759-001-a-1260">daies</w>
     <w lemma="now" pos="av" xml:id="A75759-001-a-1270">now</w>
     <w lemma="next" pos="ord" xml:id="A75759-001-a-1280">next</w>
     <w lemma="ensue" pos="vvg" xml:id="A75759-001-a-1290">ensuing</w>
     <w lemma="to" pos="acp" xml:id="A75759-001-a-1300">to</w>
     <w lemma="the" pos="d" xml:id="A75759-001-a-1310">the</w>
     <w lemma="say" pos="j-vn" xml:id="A75759-001-a-1320">said</w>
     <w lemma="committee" pos="n1" xml:id="A75759-001-a-1330">Committee</w>
     <pc unit="sentence" xml:id="A75759-001-a-1340">.</pc>
    </p>
    <p xml:id="A75759-e80">
     <w lemma="and" pos="cc" xml:id="A75759-001-a-1350">And</w>
     <w lemma="in" pos="acp" xml:id="A75759-001-a-1360">in</w>
     <w lemma="the" pos="d" xml:id="A75759-001-a-1370">the</w>
     <w lemma="same" pos="d" xml:id="A75759-001-a-1380">same</w>
     <w lemma="return" pos="n1" reg="return" xml:id="A75759-001-a-1390">returne</w>
     <w lemma="they" pos="pns" xml:id="A75759-001-a-1400">they</w>
     <w lemma="be" pos="vvb" xml:id="A75759-001-a-1410">are</w>
     <w lemma="to" pos="prt" xml:id="A75759-001-a-1420">to</w>
     <w lemma="express" pos="vvi" xml:id="A75759-001-a-1430">express</w>
    </p>
    <list xml:id="A75759-e90">
     <item xml:id="A75759-e100">
      <w lemma="1." pos="crd" xml:id="A75759-001-a-1440">1.</w>
      <pc unit="sentence" xml:id="A75759-001-a-1450"/>
      <w lemma="who" pos="crq" xml:id="A75759-001-a-1460">Whom</w>
      <w lemma="they" pos="pns" xml:id="A75759-001-a-1470">they</w>
      <w lemma="conceive" pos="vvb" xml:id="A75759-001-a-1480">conceive</w>
      <w lemma="to" pos="prt" xml:id="A75759-001-a-1490">to</w>
      <w lemma="be" pos="vvi" xml:id="A75759-001-a-1500">be</w>
      <w lemma="able" pos="j" xml:id="A75759-001-a-1510">able</w>
      <w lemma="and" pos="cc" xml:id="A75759-001-a-1520">and</w>
      <w lemma="have" pos="vvb" xml:id="A75759-001-a-1530">have</w>
      <w lemma="not" pos="xx" xml:id="A75759-001-a-1540">not</w>
      <w lemma="pay" pos="vvn" xml:id="A75759-001-a-1550">paid</w>
      <pc unit="sentence" xml:id="A75759-001-a-1560">.</pc>
     </item>
     <item xml:id="A75759-e110">
      <w lemma="2." pos="crd" xml:id="A75759-001-a-1570">2.</w>
      <pc unit="sentence" xml:id="A75759-001-a-1580"/>
      <w lemma="who" pos="crq" xml:id="A75759-001-a-1590">Who</w>
      <w lemma="be" pos="vvb" xml:id="A75759-001-a-1600">are</w>
      <w lemma="poor" pos="j" reg="poor" xml:id="A75759-001-a-1610">poore</w>
      <w lemma="and" pos="cc" xml:id="A75759-001-a-1620">and</w>
      <w lemma="unable" pos="j" xml:id="A75759-001-a-1630">unable</w>
      <w lemma="to" pos="prt" xml:id="A75759-001-a-1640">to</w>
      <w lemma="pay" pos="vvi" xml:id="A75759-001-a-1650">pay</w>
      <pc unit="sentence" xml:id="A75759-001-a-1660">.</pc>
     </item>
     <item xml:id="A75759-e120">
      <w lemma="3." pos="crd" xml:id="A75759-001-a-1670">3.</w>
      <pc unit="sentence" xml:id="A75759-001-a-1680"/>
      <w lemma="who" pos="crq" xml:id="A75759-001-a-1690">Who</w>
      <w lemma="be" pos="vvb" xml:id="A75759-001-a-1700">are</w>
      <w lemma="dead" pos="j" xml:id="A75759-001-a-1710">dead</w>
      <w lemma="and" pos="cc" xml:id="A75759-001-a-1720">and</w>
      <w lemma="left" pos="j" xml:id="A75759-001-a-1730">left</w>
      <w lemma="sufficient" pos="j" xml:id="A75759-001-a-1740">sufficient</w>
      <w lemma="estate" pos="n2" xml:id="A75759-001-a-1750">estates</w>
      <pc xml:id="A75759-001-a-1760">,</pc>
      <w lemma="and" pos="cc" xml:id="A75759-001-a-1770">and</w>
      <w lemma="who" pos="crq" xml:id="A75759-001-a-1780">who</w>
      <w lemma="be" pos="vvb" xml:id="A75759-001-a-1790">are</w>
      <w lemma="their" pos="po" xml:id="A75759-001-a-1800">their</w>
      <w lemma="executor" pos="n2" xml:id="A75759-001-a-1810">Executors</w>
      <pc xml:id="A75759-001-a-1820">,</pc>
      <w lemma="or" pos="cc" xml:id="A75759-001-a-1830">or</w>
      <w lemma="administrator" pos="n2" xml:id="A75759-001-a-1840">Administrators</w>
      <pc xml:id="A75759-001-a-1850">,</pc>
      <w lemma="and" pos="cc" xml:id="A75759-001-a-1860">and</w>
      <w lemma="where" pos="crq" xml:id="A75759-001-a-1870">where</w>
      <w lemma="they" pos="pns" xml:id="A75759-001-a-1880">they</w>
      <w lemma="dwell" pos="vvb" xml:id="A75759-001-a-1890">dwell</w>
      <pc unit="sentence" xml:id="A75759-001-a-1900">.</pc>
     </item>
     <item xml:id="A75759-e130">
      <w lemma="4." pos="crd" xml:id="A75759-001-a-1910">4.</w>
      <pc unit="sentence" xml:id="A75759-001-a-1920"/>
      <w lemma="who" pos="crq" xml:id="A75759-001-a-1930">Who</w>
      <w lemma="be" pos="vvb" xml:id="A75759-001-a-1940">are</w>
      <w lemma="remove" pos="vvn" xml:id="A75759-001-a-1950">removed</w>
      <w lemma="since" pos="acp" xml:id="A75759-001-a-1960">since</w>
      <w lemma="their" pos="po" xml:id="A75759-001-a-1970">their</w>
      <w lemma="assessment" pos="n2" reg="assessments" xml:id="A75759-001-a-1980">Assessements</w>
      <w lemma="make" pos="vvn" xml:id="A75759-001-a-1990">made</w>
      <pc xml:id="A75759-001-a-2000">,</pc>
      <w lemma="and" pos="cc" xml:id="A75759-001-a-2010">and</w>
      <w lemma="be" pos="vvb" xml:id="A75759-001-a-2020">are</w>
      <w lemma="able" pos="j" xml:id="A75759-001-a-2030">able</w>
      <pc xml:id="A75759-001-a-2040">,</pc>
      <w lemma="and" pos="cc" xml:id="A75759-001-a-2050">and</w>
      <w lemma="have" pos="vvb" xml:id="A75759-001-a-2060">have</w>
      <w lemma="not" pos="xx" xml:id="A75759-001-a-2070">not</w>
      <w lemma="pay" pos="vvn" xml:id="A75759-001-a-2080">paid</w>
      <pc xml:id="A75759-001-a-2090">,</pc>
      <w lemma="and" pos="cc" xml:id="A75759-001-a-2100">and</w>
      <w lemma="where" pos="crq" xml:id="A75759-001-a-2110">where</w>
      <w lemma="they" pos="pns" xml:id="A75759-001-a-2120">they</w>
      <w lemma="now" pos="av" xml:id="A75759-001-a-2130">now</w>
      <w lemma="dwell" pos="vvi" xml:id="A75759-001-a-2140">dwell</w>
      <pc unit="sentence" xml:id="A75759-001-a-2150">.</pc>
     </item>
     <item xml:id="A75759-e140">
      <w lemma="5." pos="crd" xml:id="A75759-001-a-2160">5.</w>
      <pc unit="sentence" xml:id="A75759-001-a-2170"/>
      <w lemma="such" pos="d" xml:id="A75759-001-a-2180">Such</w>
      <w lemma="landlord" pos="n2" xml:id="A75759-001-a-2190">Landlords</w>
      <w lemma="as" pos="acp" xml:id="A75759-001-a-2200">as</w>
      <w lemma="be" pos="vvb" xml:id="A75759-001-a-2210">are</w>
      <w lemma="assess" pos="vvn" xml:id="A75759-001-a-2220">assessed</w>
      <w lemma="and" pos="cc" xml:id="A75759-001-a-2230">and</w>
      <w lemma="have" pos="vvb" xml:id="A75759-001-a-2240">have</w>
      <w lemma="not" pos="xx" xml:id="A75759-001-a-2250">not</w>
      <w lemma="pay" pos="vvn" xml:id="A75759-001-a-2260">paid</w>
      <pc xml:id="A75759-001-a-2270">,</pc>
      <w lemma="and" pos="cc" xml:id="A75759-001-a-2280">and</w>
      <w lemma="their" pos="po" xml:id="A75759-001-a-2290">their</w>
      <w lemma="tenant" pos="n2" xml:id="A75759-001-a-2300">Tenants</w>
      <w lemma="remove" pos="vvn" xml:id="A75759-001-a-2310">removed</w>
      <pc xml:id="A75759-001-a-2320">,</pc>
      <w lemma="then" pos="av" xml:id="A75759-001-a-2330">then</w>
      <w lemma="to" pos="prt" xml:id="A75759-001-a-2340">to</w>
      <w lemma="certify" pos="vvi" reg="certify" xml:id="A75759-001-a-2350">certifie</w>
      <w lemma="the" pos="d" xml:id="A75759-001-a-2360">the</w>
      <w lemma="name" pos="n2" xml:id="A75759-001-a-2370">names</w>
      <w lemma="of" pos="acp" xml:id="A75759-001-a-2380">of</w>
      <w lemma="the" pos="d" xml:id="A75759-001-a-2390">the</w>
      <w lemma="new" pos="j" xml:id="A75759-001-a-2400">new</w>
      <w lemma="tenant" pos="n2" xml:id="A75759-001-a-2410">Tenants</w>
      <pc unit="sentence" xml:id="A75759-001-a-2420">.</pc>
     </item>
    </list>
    <p xml:id="A75759-e150">
     <w lemma="all" pos="av-d" xml:id="A75759-001-a-2430">All</w>
     <w lemma="which" pos="crq" xml:id="A75759-001-a-2440">which</w>
     <w lemma="particular" pos="n2-j" xml:id="A75759-001-a-2450">particulars</w>
     <w lemma="be" pos="vvb" xml:id="A75759-001-a-2460">are</w>
     <w lemma="by" pos="acp" xml:id="A75759-001-a-2470">by</w>
     <w lemma="they" pos="pno" xml:id="A75759-001-a-2480">them</w>
     <w lemma="to" pos="prt" xml:id="A75759-001-a-2490">to</w>
     <w lemma="be" pos="vvi" xml:id="A75759-001-a-2500">be</w>
     <w lemma="careful" pos="av-j" xml:id="A75759-001-a-2510">carefully</w>
     <w lemma="perform" pos="vvn" xml:id="A75759-001-a-2520">performed</w>
     <pc xml:id="A75759-001-a-2530">,</pc>
     <w lemma="to" pos="acp" xml:id="A75759-001-a-2540">to</w>
     <w lemma="the" pos="d" xml:id="A75759-001-a-2550">the</w>
     <w lemma="end" pos="n1" xml:id="A75759-001-a-2560">end</w>
     <pc xml:id="A75759-001-a-2570">,</pc>
     <w lemma="that" pos="cs" xml:id="A75759-001-a-2580">that</w>
     <w lemma="what" pos="crq" xml:id="A75759-001-a-2590">what</w>
     <w lemma="arrear" pos="n2" xml:id="A75759-001-a-2600">Arrears</w>
     <w lemma="can" pos="vmbx" xml:id="A75759-001-a-2610">cannot</w>
     <w lemma="be" pos="vvi" xml:id="A75759-001-a-2620">be</w>
     <w lemma="collect" pos="vvn" xml:id="A75759-001-a-2630">collected</w>
     <pc xml:id="A75759-001-a-2640">,</pc>
     <w lemma="may" pos="vmb" xml:id="A75759-001-a-2650">may</w>
     <w lemma="be" pos="vvi" xml:id="A75759-001-a-2660">be</w>
     <w lemma="return" pos="vvn" xml:id="A75759-001-a-2670">returned</w>
     <w lemma="back" pos="av" xml:id="A75759-001-a-2680">back</w>
     <w lemma="to" pos="acp" xml:id="A75759-001-a-2690">to</w>
     <w lemma="the" pos="d" xml:id="A75759-001-a-2700">the</w>
     <w lemma="respective" pos="j" xml:id="A75759-001-a-2710">respective</w>
     <w lemma="ward" pos="n2" xml:id="A75759-001-a-2720">Wards</w>
     <w lemma="to" pos="prt" xml:id="A75759-001-a-2730">to</w>
     <w lemma="be" pos="vvi" xml:id="A75759-001-a-2740">be</w>
     <w lemma="new" pos="av-j" xml:id="A75759-001-a-2750">newly</w>
     <w lemma="assess" pos="vvn" xml:id="A75759-001-a-2760">assessed</w>
     <pc unit="sentence" xml:id="A75759-001-a-2770">.</pc>
    </p>
    <closer xml:id="A75759-e160">
     <signed xml:id="A75759-e170">
      <w lemma="tho." pos="ab" xml:id="A75759-001-a-2780">Tho.</w>
      <w lemma="n/a" pos="fla" xml:id="A75759-001-a-2790">Lathum</w>
      <w lemma="clerk" pos="n1" xml:id="A75759-001-a-2800">Clerk</w>
      <w lemma="to" pos="acp" xml:id="A75759-001-a-2810">to</w>
      <w lemma="the" pos="d" xml:id="A75759-001-a-2820">the</w>
      <w lemma="say" pos="j-vn" xml:id="A75759-001-a-2830">said</w>
      <w lemma="committee" pos="n1" xml:id="A75759-001-a-2840">Committee</w>
      <pc unit="sentence" xml:id="A75759-001-a-2850">.</pc>
     </signed>
    </closer>
   </div>
  </body>
 </text>
</TEI>
